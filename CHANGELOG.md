### 2021-02-01 - tag : cpu-pt1.7-ts2.3.1-dgm0.4

  - add docker-entrypoint.sh for better fix permission


### 2021-01-28 - tag : cpu-pt1.7-ts2.3.1-dgm0.3

  - fix permission for data volume mount

### 2021-01-28 - tag : cpu-pt1.7-ts2.3.1-dgm0.2

  - rewrite Dockerfile for run jupyter in /home/$NB_USER dir (aka /home/jovyan)
